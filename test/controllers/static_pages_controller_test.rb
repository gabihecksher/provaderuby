require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase

  test "should get fist" do
    get :first
    assert_response :success
  end

  test "should get second" do
    get :second
    assert_response :success
  end
end